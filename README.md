
#### 这是什么

这是基于 51 单片机语音时钟的代码

### 按键定义

|         |  按键 0 单击   | 按键 0 长按  | 按键 1 单击 | 按键 1 长按    | 按键 2 单击 | 按键 2 长按    |
| :-----: | :------------: | :----------: | ----------- | -------------- | ----------- | -------------- |
| 0：正常 |    语音报时    |    进入 1    | 语音报温度  |                | 语音报湿度  |                |
| 1：调时 | 保存时，进入 2 | 保存，退到 0 | 时 +        | 不保存，进入 2 | 时 -        | 不保存，退到 0 |
| 2：调分 | 保存分，进入 3 | 保存，退到 0 | 分 +        | 不保存，进入 3 | 分 -        | 不保存，退到 0 |
| 3：调秒 | 保存秒，进入 1 | 保存，退到 0 | 秒归零      | 不保存，进入 1 | 秒归零      | 不保存，退到 0 |


### 函数返回值代表含义

| 返回值 | 函数执行情况 | 返回数据 |
| :----: | :----------: | :------: |
|   0    |   执行成功   |  数据 0  |
|   1    |              |  数据 1  |
|   -1   |   执行错误   |          |
|  其他  |              |   其他   |
